#!/usr/bin/python
import os
import json
from collections import OrderedDict

print ("Sortable Programming challenge")

products = []
listings = []
matches = {}

#Get list of products
with open('products.txt') as product_file:
    for line in product_file:
        products.append(json.loads(line))
        #print(json.loads(line))


#Get list of listings
with open('listings.txt') as listings_file:
    for line in listings_file:
        #listings.append(json.loads(line)) Instead of loading directly , I am converting into an ordered list to maintain its sequence
        #print(json.loads(line))
        cur_listing = OrderedDict([('title',json.loads(line)['title']), 
                          ('manufacturer', json.loads(line)['manufacturer']),
                          ('currency', json.loads(line)['currency']),
                          ('price', json.loads(line)['price'])])
        listings.append(cur_listing)
        
        
        
#Perform comparisons using upper case of all strings cause case might differ
for product in products:
    list = []
    for listing in listings:
        if product['manufacturer'].upper() in listing['title'].upper() and product['model'].upper() in listing['title'].upper() :
            list.append(listing)
    matches[product['product_name']] = list  
  

# delete only if file exists as we are appending  #
if os.path.exists('data.txt'):
    os.remove('data.txt')

#dump dictionary to output file
with open('data.txt', 'a') as result:
    for productId in matches.keys():
        resultObject = OrderedDict([('product_name',productId), 
                          ('listings', matches[productId])])
       
        json.dump(resultObject, result)  
        result.write('\n')
    
            
        
    